﻿using M4DBO;
using M4UIO;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace UBA.Mesap.AdminHelper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// Basic application class. Organizes database access (API and non-API).
    /// </summary>
    public partial class AdminHelper : Application, IDisposable
    {
        // API base objects 
        internal dboRoot root;
        internal uioRoot uiRoot;
        internal dboDatabase database;

        // Direct non-API database access
        private SqlConnection databaseConnection;

        // MESAP client server system connection settings
        bool readOnly = false;
        bool exclusive = false;

        protected override void OnStartup(StartupEventArgs e)
        {
            String defaultDatabaseId = "ZSE_aktuell";
            mspErrUioInitEnum rootErr;

            uiRoot = new M4UIO.uioRoot();
            rootErr = uiRoot.Initialize("", "", mspM4AppEnum.mspM4AppOEM, false, "UBA Mesap Admin Helper");

            if (rootErr == mspErrUioInitEnum.mspErrNone)
            {
                uiRoot.Dbo.Login(Private.MesapAdminUsername, Private.MesapAdminPassword);
                root = uiRoot.Dbo;
                Connect(defaultDatabaseId, readOnly, exclusive);
            }
            else throw new NullReferenceException("MESAP Initialization failed! Error: " + rootErr);
        }

        /// <summary>
        /// Switch to database with given identifier. Effects both API and
        /// non-API access.
        /// </summary>
        /// <param name="id">ID of database to switch to. Do not give non-existent id!</param>
        /// <returns>Whether switch was successful (true) or failed (false).</returns>
        public bool SwitchDatabase(String id)
        {
            if (!CanSwitchDatabase(id)) return false;
            return Connect(id, readOnly, exclusive);
        }

        /// <summary>
        /// Check whether the database is usable with the admin tool,
        /// i.e. we do have the connection details. Does NOT switch!
        /// </summary>
        /// <param name="id">ID of database.</param>
        /// <returns>Whether switch is potentially possible (true) or not (false).</returns>
        public bool CanSwitchDatabase(String id)
        {
            return BuildDBConnectionString(id) != null;
        }

        public void Dispose()
        {
            databaseConnection.Dispose();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            root.Logout();
            databaseConnection.Close();

            base.OnExit(e);
        }

        /// <summary>
        /// Gets a direct SQL (non-API) database connection.
        /// </summary>
        /// <returns>Connection to database, already opened.</returns>
        internal SqlConnection GetDirectDBConnection()
        {
            if (databaseConnection.State == ConnectionState.Closed ||
                databaseConnection.State == ConnectionState.Broken)
                databaseConnection.Open();

            return databaseConnection;
        }

        /// <summary>
        /// Get a direct SQL (non-API) database connection.
        /// </summary>
        /// <param name="databaseId">Database you want to connect to. Can be different to the current default.</param>
        /// <returns>New and closed SQL connection object, to be managed by the caller.</returns>
        internal SqlConnection GetDirectDBConnection(String databaseId)
        {
            return new SqlConnection(BuildDBConnectionString(databaseId));
        }

        private bool Connect(String databaseId, bool readOnly, bool exclusive)
        {
            // Open API connection
            if (database != null)
            {
                root.Databases.CloseDb(database.DbNr);
                database = null;
            }

            mspErrDboOpenDbEnum databaseErr = root.Databases.OpenDb(databaseId, readOnly, ref exclusive);

            if (databaseErr == mspErrDboOpenDbEnum.mspErrNone)
                database = root.MainDb;
            else throw new Exception("Failed to connect to database " + databaseId + ": " + databaseErr);

            // Open non-API connection
            if (databaseConnection != null && databaseConnection.State != ConnectionState.Closed)
                databaseConnection.Close();

            databaseConnection = new SqlConnection(BuildDBConnectionString(databaseId));

            return databaseErr == mspErrDboOpenDbEnum.mspErrNone;
        }

        private String BuildDBConnectionString(String databaseId)
        {
            String databaseName;
            switch (databaseId)
            {
                case "ESz":
                case "BEU":
                case "Enerdat":
                    databaseName = databaseId.ToUpper(); break;
                case "ZSE_aktuell":
                case "EMMa_2018":
                case "EMMa_2020":
                case "EMMa_2021":
                case "EMMa_2022":
                case "EMMa_2023":
                    databaseName = databaseId; break;
                case "ZSE_2024":
                case "ZSE_2023":
                case "ZSE_2022":
                case "ZSE_2021":
                case "ZSE_2020":
                case "ZSE_2019":
                case "ZSE_2018":
                case "ZSE_2017":
                case "ZSE_2016":
                case "ZSE_2015":
                case "ZSE_2014":
                case "ZSE_2013":
                case "ZSE_2012":
                case "ZSE_2011":
                case "ZSE_2010":
                case "ZSE_2009":
                case "ZSE_2008":
                case "ZSE_2007":
                case "ZSE_2006":
                case "ZSE_2005":
                case "ZSE_2004":
                case "ZSE_2003":
                    databaseName = "ZSE_Submission_" + databaseId.Substring(4, 4); break;
                case "PoSo_aktuell":
                    databaseName = "PoSo"; break;
                default:
                    Console.WriteLine("OOPS: Connection string for unknown database \"" + databaseId + "\" requested");
                    return null;
            }

            return String.Format(Private.MesapServerConnectionTemplate, Private.MesapServerName, databaseName, Private.MesapServerUsername, Private.MesapServerPassword);
        }
    }
}
